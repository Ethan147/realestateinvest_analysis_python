from peewee import *
import datetime
import math

# NOTE:
    # quandl API key: xEX4HMfzk5XhLf63cK7q
    # NOTE: look into corelogic - call them (number should be bookmarked)

######################################
# db Model creation & definition ####
######################################

# Connect to real estate database
db = MySQLDatabase('real_estate', user='app', password='db_password',
                         host='127.0.0.1', port=3306)

# Base model class defines which database to use. Then, any subclasses will automatically use the correct storage
#  see https://docs.peewee-orm.com/en/latest/peewee/example.html
class BaseModel(Model):
    class Meta:
        database = db

# Basic values about a property to record for analysis, includes only the current/most recent YYYYMM value for the property
# Value for property prior to the current YYYYMM will be moved to a separate, more compact table
class Property(BaseModel):

    # General USPS identifiers (do not allow null for this)
    streetAddress = DecimalField(max_digits=9,decimal_places=0)
    streetName = CharField(max_length=28)
    stateCode = CharField(max_length=2)
    zipCode = DecimalField(max_digits=5,decimal_places=0)
    city = CharField(max_length=20)

    # GPS identifier
    gpsLat = DecimalField(max_digits=7,decimal_places=5,null=True,default=None) # latitude (-90.00000 to 90.00000)
    gpsLon = DecimalField(max_digits=8,decimal_places=5,null=True,default=None) # longitude (-180.00000 to 180.00000)

    # building type identifiers
    sqft = DecimalField(max_digits=4,decimal_places=0,null=True,default=None)
    stories = DecimalField(max_digits=1,decimal_places=0,null=True,default=None)
    rooms = DecimalField(max_digits=1,decimal_places=0,null=True,default=None)
    bathrooms = DecimalField(max_digits=1,decimal_places=0,null=True,default=None)
    buildyear = DecimalField(max_digits=4,decimal_places=0,null=True,default=None)
    numberFamilies = DecimalField(max_digits=3,decimal_places=0,null=True,default=None) # 1 = SFH, 2 = Duplex, etc
    numberBuildings = DecimalField(max_digits=1,decimal_places=0,null=True,default=None) # 1 = SFH, 2 = Duplex, etc

    # API scores
    walkScore = DecimalField(max_digits=3,decimal_places=0,null=True,default=None)
    crimeScore = DecimalField(max_digits=3,decimal_places=0,null=True,default=None)
    schoolScore = DecimalField(max_digits=3,decimal_places=0,null=True,default=None)

    # Monthly Expenses
    HOADues = DecimalField(max_digits=3,decimal_places=0,null=True,default=None)
    PropertyTaxes = DecimalField(max_digits=3,decimal_places=0,null=True,default=None)
    Leins = DecimalField(max_digits=4,decimal_places=0,null=True,default=None) # divided by 10 for space savings, so range is $0 to $99,990

    # Property Value & Rent value gathered for the month
    # NOTE: Value entry in database is divided by 100 for space savings (real digits is 8, += $100 is noise)
    # NOTE: Rent entry in database is divided by 10 for space savings (real digits is 4, +- $10 is noise)
    presentValue = DecimalField(max_digits=6,decimal_places=0,null=True,default=None)
    presentRent = DecimalField(max_digits=3,decimal_places=0,null=True,default=None)
    presentMonth = DateField(formats='%Y-%m-%d',null=True,default=None) # only to be updated when accurate value information is available

    class Meta:
        primary_key = CompositeKey('streetAddress','streetName','stateCode','zipCode','city')
        indexes = (
        ( ('streetAddress','streetName','stateCode','zipCode','city'), True ), 
        )


# Good practice to connect & disconnect on every use
db.connect()

db.create_tables([Property]) #,PropertyValueHistory,PropertyRentHistory])


######################################
# General Helper Classes #############
######################################

class dbNumberTranslator:

    def __init__(self):
        return

    def pushValue(self, value):
        if value is not None:
            pushVal = math.ceil(value / 100) * 100 # Round to the nearest 100
            return pushVal/100
        else:
            return None

    def pullValue(self, value):
        if value is not None:
            return value*100
        else:   
            return None

    def pushRent(self, rent):
        if rent is not None:
            pushRen = math.ceil(rent / 10) * 10 # Rount to the nearest 10
            return pushRen/10
        else:
            return None

    def pullRent(self, rent):
        return rent*10

    def pushDate(self): # only required for pushing, pulling will return a valid date for analysis
        return datetime.datetime.today().replace(day=1).strftime('%Y-%m-%d')

class QueryType():
        Insert = 1
        Update = 2

######################################
# User Interactions with db ##########
######################################

class objProperty(object):

    def __init__(self,
                streetAddress, streetName, stateCode, zipCode, city, gpsLat, gpsLon, 
                sqft, stories, rooms, bathrooms, buildyear, numberFamilies, numberBuildings,
                HOADues, PropertyTaxes, Leins,
                walkScore, crimeScore, schoolScore,
                presentValue, presentRent):#, presentMonth):

        self.dbTranslate = dbNumberTranslator()        
        self.streetAddress=streetAddress
        self.streetName=streetName
        self.stateCode=stateCode
        self.zipCode=zipCode
        self.city=city
        self.gpsLat=gpsLat
        self.gpsLon=gpsLon 
        self.sqft=sqft
        self.stories=stories
        self.rooms=rooms
        self.bathrooms=bathrooms
        self.buildyear=buildyear
        self.numberFamilies=numberFamilies
        self.numberBuildings=numberBuildings
        self.walkScore=walkScore
        self.crimeScore=crimeScore
        self.schoolScore=schoolScore
        self.HOADues=HOADues
        self.PropertyTaxes=PropertyTaxes
        self.Leins=Leins
        
        # Values that need translating, for space savings
        self.presentValue=self.dbTranslate.pushValue(presentValue)
        self.presentRent=self.dbTranslate.pushRent(presentRent)
        self.presentMonth=self.dbTranslate.pushDate() #(presentMonth) -- NOTE: will have to think if it's worth keeping presetdate around

class dbInteractions:

    def __init__(self):
        return

    def dbGetZipList(self, zip):
        query = Property.select().where(Property.zipCode == zip)
        zipList = list(query)
        return zipList

    def dbGetSA(self):
        query = Property.select().where(Property.city == 'San Antonio')
        zipList = list(query)
        return zipList

    # For inserting or updating a single property
    def dbSingleInsertOrUpdate(self, objProperty):
        try:
            with db.atomic():
                Property.create( 
                            streetAddress=objProperty.streetAddress, streetName=objProperty.streetName, stateCode=objProperty.stateCode,
                            zipCode=objProperty.zipCode, city=objProperty.city, gpsLat=objProperty.gpsLat, gpsLon=objProperty.gpsLon,
                            sqft=objProperty.sqft, stories=objProperty.stories, rooms=objProperty.rooms, bathrooms=objProperty.bathrooms,
                            buildyear=objProperty.buildyear, numberFamilies=objProperty.numberFamilies, numberBuildings=objProperty.numberBuildings,
                            HOADues=objProperty.HOADues, PropertyTaxes=objProperty.PropertyTaxes, Leins=objProperty.Leins,                            
                            walkScore=objProperty.walkScore, crimeScore=objProperty.crimeScore, schoolScore=objProperty.schoolScore, 
                            presentValue=objProperty.presentValue, presentRent=objProperty.presentRent, presentMonth=objProperty.presentMonth 
                            )
                return QueryType.Insert

        except IntegrityError:

            query = Property.update(
                            gpsLat=objProperty.gpsLat, gpsLon=objProperty.gpsLon,
                            sqft=objProperty.sqft, stories=objProperty.stories, rooms=objProperty.rooms, bathrooms=objProperty.bathrooms,
                            buildyear=objProperty.buildyear, numberFamilies=objProperty.numberFamilies, numberBuildings=objProperty.numberBuildings,
                            HOADues=objProperty.HOADues, PropertyTaxes=objProperty.PropertyTaxes, Leins=objProperty.Leins,                            
                            walkScore=objProperty.walkScore, crimeScore=objProperty.crimeScore, schoolScore=objProperty.schoolScore, 
                            presentValue=objProperty.presentValue, presentRent=objProperty.presentRent, presentMonth=objProperty.presentMonth 
                            ).where(
                                    Property.streetAddress==objProperty.streetAddress, Property.streetName==objProperty.streetName, Property.stateCode==objProperty.stateCode,
                                    Property.zipCode==objProperty.zipCode, Property.city==objProperty.city)
            query.execute()
            return QueryType.Update

    # For inserting a multitude of properties
    def dbMultipleBasicInsert(self, propertyDictionaryList):
        try:
            with db.atomic():        
                Property.insert_many(propertyDictionaryList).execute()

        except IntegrityError:
            return # eventually add an identifier for what failed


# testProperty = objProperty( 
#     streetAddress = 12,
#     streetName = 'Fake Street',
#     stateCode = 'TX',
#     zipCode = 78660,
#     city = 'Austin',
#     gpsLat = -97.743,
#     gpsLon = 30.267,
#     sqft = 777,
#     stories = 1,
#     rooms = 2,
#     bathrooms = 1, 
#     buildyear = 1001,
#     numberFamilies = 88,
#     walkScore = 60,
#     crimeScore = 70,
#     schoolScore = 80,
#     HOADues = 0,
#     PropertyTaxes = 200,
#     Leins = 0,
#     presentValue = 300000,
#     presentRent = 120, # 1200 is out of range, should find a way to solve this
#     # presentMonth = datetime.datetime.today().replace(day=1).strftime('%Y-%m-%d') 
#     )

dbInter = dbInteractions()

