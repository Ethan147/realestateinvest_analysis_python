import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


class PropertyElement:

    def __init__(self, inYearAdd, inPropertyValue, inNumberOfFamily, inAverageMonthlyRent, inOccupancyPercentage, inAnnualValueAppreciationPercentage,
                inMonthlyUtilities, inMonthlyHOAFees, inMonthlyInsurance, inPercentageYearlyTaxes, inPercentageAnnualMaintenance, 
                inDownpayment, inMortgageInterest, inMortgageYears,           
                inManaged, inManagementFeePercentage
                ):
        self.yearAdd=inYearAdd
        self.propertyValue=inPropertyValue
        self.numberOfFamily=inNumberOfFamily
        self.averageMonthlyRent=inAverageMonthlyRent
        self.occupancyPercentage=inOccupancyPercentage
        self.annualValueAppreciationPercentage=inAnnualValueAppreciationPercentage
        self.monthlyUtilities=inMonthlyUtilities
        self.monthlyHOAFees=inMonthlyHOAFees
        self.percentageYearlyTaxes=inPercentageYearlyTaxes
        self.percentageAnnualMaintenance=inPercentageAnnualMaintenance 
        self.downpayment=inDownpayment
        self.mortgageInterest=inMortgageInterest
        self.mortgageYears=inMortgageYears          
        self.managed=inManaged
        self.managementFeePercentage=inManagementFeePercentage
        self.propertyOwnedValueWithAppr = 0
        self.percentMortgagePaidOff = 0
        # self.mortgagePrincipalPaidOff = 0
        self.monthlyInsurance = inMonthlyInsurance
        self.propertyPrincipalPayedOff = (1-inDownpayment)*inPropertyValue

class Graph:

    def __init__(self, inPropList):
        self.propList = inPropList

    def _clearMemory(self):
        plt.clf()

    def plotList(self, list, listLabel, chartTitle):
        ser = pd.Series(list)    
        ser.plot(label=listLabel)
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})
        plt.title(chartTitle, loc='center', fontsize = 6)

    def titleProperties(self):
        for x in range(0,len(self.propList)):
            totalMiscCost = self.propList[x].monthlyUtilities + self.propList[x].monthlyHOAFees + self.propList[x].monthlyInsurance
            totalMiscCost = 'Monthly Utilites + HOA Fees + Insurance:' + str(totalMiscCost)
            mortgageDetails = 'Mortgage Years, downpayment, Interest: (' + str(self.propList[x].mortgageYears) + ', ' + str(self.propList[x].downpayment) + ', ' + str(self.propList[x].mortgageInterest) + '), '
            managedDetails = 'Managed, Management %: (' + str(self.propList[x].managed) + ', ' + str(self.propList[x].managementFeePercentage) + '), '  
            propertyDetails = 'Value, YearAdd, # Family, Avg Rent, Occ. %, Appr. %: (' + str(self.propList[x].propertyValue) + ', ' + str(self.propList[x].yearAdd) + ', ' + str(self.propList[x].numberOfFamily) + ', ' + str(self.propList[x].averageMonthlyRent) + ', ' + str(self.propList[x].occupancyPercentage) + ', ' + str(self.propList[x].annualValueAppreciationPercentage) + '), '
            percentageFeeDetails = 'Tax %, Maintenance %: (' + str(self.propList[x].percentageYearlyTaxes) + ', ' + str(self.propList[x].percentageAnnualMaintenance) + '), ' 
        
            HouseDetails = propertyDetails + mortgageDetails + managedDetails + percentageFeeDetails + totalMiscCost
            AllDetails = HouseDetails + '\n'
        
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})
        plt.title(AllDetails, loc='center', fontsize = 5)

    def plotTotalAssets(self, totalAssets):
        ser = pd.Series(totalAssets)    
        ser.plot(label='total assets')
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})

    def plotMarketAssets(self, marketAssets):
        ser = pd.Series(marketAssets)    
        ser.plot(label='market assets')
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})

    def plotPropertyAssets(self, propertyAssets):
        ser = pd.Series(propertyAssets)    
        ser.plot(label='property assets')
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})

    def plotAnnualCashflow(self, cashFlowList):
        ser = pd.Series(cashFlowList)    
        ser.plot(label='cash flow')
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})

    def save(self, fileName):
        plt.savefig(fileName, dpi=600)
        self._clearMemory()

class GraphSimple:

    def _clearMemory(self):
        plt.clf()

    def plotList(self, list, listLabel, chartTitle):
        ser = pd.Series(list)    
        ser.plot(label=listLabel)
        plt.legend(bbox_to_anchor=(1, 1),
            bbox_transform=plt.gcf().transFigure)
        plt.legend(loc=2, prop={'size': 6})
        plt.title(chartTitle, loc='center', fontsize = 6)

    def save(self, fileName):
        plt.savefig(fileName, dpi=600)
        self._clearMemory()

class PropertyHandle:

    def __init__(self, startingCapital, inflation, years, avgMarketPercentageReturns, savedIncome):
        self.propertyList = [PropertyElement(
                                inYearAdd=-1, 
                                inPropertyValue=-1, 
                                inNumberOfFamily=-1, 
                                inAverageMonthlyRent=-1, 
                                inOccupancyPercentage=-1, 
                                inAnnualValueAppreciationPercentage=-1,
                                inMonthlyUtilities=-1, 
                                inMonthlyHOAFees=-1, 
                                inMonthlyInsurance=-1, 
                                inPercentageYearlyTaxes=-1, 
                                inPercentageAnnualMaintenance=-1, 
                                inDownpayment=-1, 
                                inMortgageInterest=-1, 
                                inMortgageYears=-1,           
                                inManaged=-1, 
                                inManagementFeePercentage=-1) for _ in range(1)]
        self.marketAssets = []
        self.marketAssets.append(startingCapital)
        self.propertyAssets = []
        self.propertyAssets.append(0)
        self.totalAssets = []
        self.debt = []
        self.totalAssets.append(self.propertyAssets[0] + self.marketAssets[0])
        self.annualCashFlow = []
        self.annualCashFlow.append(0)
        self.inflation = inflation
        self.startingCapital = startingCapital
        self.years = years
        self.marketPercentageReturn = avgMarketPercentageReturns
        self.bearMarketDownturnPercentage = 0.04
        self.savedIncome = savedIncome
        self.addPropertyCounter = 0

    def addProperty(self, propertyValue, yearAdd, downpayment, numberOfFamily, averageMonthlyRent, 
                    occupancyPercentage, managed, mortgageInterest, mortgageYears, monthlyUtilities, monthlyHOAFees, monthlyInsurance, 
                    percentageYearlyTaxes, percentageAnnualMaintenance, managementFeePercentage, propValAppreciationPercentage):
        
        newProperty = PropertyElement(
            inYearAdd=yearAdd, 
            inPropertyValue=propertyValue, 
            inNumberOfFamily=numberOfFamily, 
            inAverageMonthlyRent=averageMonthlyRent, 
            inOccupancyPercentage=occupancyPercentage,
            inAnnualValueAppreciationPercentage=propValAppreciationPercentage,
            inMonthlyUtilities=monthlyUtilities, 
            inMonthlyHOAFees=monthlyHOAFees, 
            inMonthlyInsurance=monthlyInsurance,
            inPercentageYearlyTaxes=percentageYearlyTaxes, 
            inPercentageAnnualMaintenance=percentageAnnualMaintenance, 
            inDownpayment=downpayment, 
            inMortgageInterest=mortgageInterest, 
            inMortgageYears=mortgageYears,           
            inManaged=managed, 
            inManagementFeePercentage=managementFeePercentage 
            )

        if self.addPropertyCounter == 0:
            self.propertyList[0] = newProperty
        else:
            self.propertyList.append(newProperty)

        self.addPropertyCounter += 1
        return

    # MUST update code to calculate the amount of principal house value that has been paid
    def simulate(self, fileName):

        for yr in range(0,self.years):
            
            if yr is not 0:
                self.marketAssets.append(0)
                self.propertyAssets.append(0)
                self.totalAssets.append(0)
                self.annualCashFlow.append(0)

            # Total cashflow for the year
            self.annualCashFlow[yr] = 0
            for x in range(0, len(self.propertyList)):
                if self.propertyList[x].yearAdd <= yr:
                    rentalAnnualIncome = self._propertyAnnualIncome(self.propertyList[x])
                    
                    if self.propertyList[x].managed:
                        rentalAnnualIncome = rentalAnnualIncome*(1-self.propertyList[x].managementFeePercentage)
                    rentalAnnualIncome -= self._monthlyMortgageFixedRate(self.propertyList[x])*12 # subtract mortgage payments from cashflow
                    rentalAnnualIncome -= (self.propertyList[x].monthlyUtilities +  self.propertyList[x].monthlyHOAFees)*12
                    rentalAnnualIncome -= (self.propertyList[x].percentageYearlyTaxes + self.propertyList[x].percentageAnnualMaintenance)*self.propertyList[x].propertyValue
                    self.annualCashFlow[yr] += rentalAnnualIncome
            
            print(str(self.marketAssets[yr]))

            # Market gains for the year
            if not yr == 0:
                
                self.marketAssets[yr] = self._annualMarket(
                                                Capital = self.marketAssets[yr-1],
                                                yearNumber = yr
                                                )        
                
                print(str(self.marketAssets[yr]))
                
            # Subtract closing costs for any property on this year
            for x in range(0, len(self.propertyList)):
                if yr == self.propertyList[x].yearAdd:
                    print('yr ' + str(yr) + 'market before ' + str(self.marketAssets[yr]))
                    # print(str(yr))
                    self.marketAssets[yr] -= self._oneTimeCost(self.propertyList[x])
                    print(' year ' + str(yr) + ', market after ' + str( self.marketAssets[yr] ))
            
            # Subtract general costs for any owned property this year, update property owned value
            for x in range(0, len(self.propertyList)):
                if self.propertyList[x].yearAdd <= yr:
                    self.marketAssets[yr] = self.marketAssets[yr] - self._annualCostMaintenance(self.propertyList[x])            
                    print(str(self.marketAssets[yr]))

                    if yr <= (self.propertyList[x].yearAdd + self.propertyList[x].mortgageYears):
                        self.marketAssets[yr] = self.marketAssets[yr] - self._monthlyMortgageFixedRate(self.propertyList[x])*12            
                        print(str(self.marketAssets[yr]))

                    self.marketAssets[yr] = self.marketAssets[yr] - self._annualPropertyManagementFees(self.propertyList[x])
                    print(str(self.marketAssets[yr]))

            # Tally the owned property value accross all properties
            #  MUST COME BACK TO THIS - PROPERTY VALUE CALCULATION IS INNACURRATE & APPR MAY be as well
            for x in range(0, len(self.propertyList)):
                if self.propertyList[x].yearAdd <= yr:
                    self._propertyOwnedPrincipalValue(self.propertyList[x], yr)
                    self.propertyAssets[yr] += self.propertyList[x].propertyOwnedValueWithAppr


            # # Tax Benefits
            # for x in range(0, len(self.propertyList)-1):
            #     self.marketAssets[yr] = self.marketAssets[yr] + self._taxBenefits(self.propertyList[x])

            # Annual income ###
            for x in range(0, len(self.propertyList)):
                if self.propertyList[x].yearAdd <= yr:
                    print(str(self.marketAssets[yr]))
                    self.marketAssets[yr] = self.marketAssets[yr] + self._propertyAnnualIncome(self.propertyList[x])
                    print(str(self.marketAssets[yr]))

            # # Annual inflation ###
            self.marketAssets[yr] = self._annualInflation(self.marketAssets[yr])
            print('caught year ' + str(yr) + ' ' + str(self.marketAssets[yr]))
            self.propertyAssets[yr] = self._annualInflation(self.propertyAssets[yr])
            self.annualCashFlow[yr] = self._annualInflation(self.annualCashFlow[yr])

            self.totalAssets[yr] = self.marketAssets[yr] + self.propertyAssets[yr]
        
        gr = Graph(self.propertyList)
        gr.plotTotalAssets(self.totalAssets)
        gr.plotPropertyAssets(self.propertyAssets)
        gr.plotMarketAssets(self.marketAssets)
        # gr.titleProperties()
        gr.save(fileName + ', Market')# Assets')

        gr.plotAnnualCashflow(self.annualCashFlow)
        gr.save(fileName + ', Cashflow')

    # MUST create this -- for now just return the same
    def _taxBenefits(self, propElem):
        return 0
    #
    def _propertyAnnualIncome(self, propElem):
        income = propElem.averageMonthlyRent*propElem.numberOfFamily*12*propElem.occupancyPercentage
        return income
    #
    def _annualInflation(self, assets):
        print(' assets ' + str(assets) + ' inflation ' + str(self.inflation) + ' multiplier ' + str(1-self.inflation))
        postInflation = (assets * (1-self.inflation))
        print(' post inflation ' + str(postInflation))
        return postInflation
    #
    def _oneTimeCost(self, propElem, closingCostPercentage = 0.04):
        upFront = propElem.propertyValue*propElem.downpayment + propElem.propertyValue*closingCostPercentage
        # print('upfront ' + str(upFront))
        return upFront
    #
    def _annualCostMaintenance(self, propElem):
    
        monthly = propElem.monthlyHOAFees + propElem.monthlyUtilities
        annual = (propElem.percentageAnnualMaintenance +  propElem.percentageYearlyTaxes) * propElem.propertyValue
        #For most homeowners, the annual costs for a homeowners insurance policy can be estimated by 
        # dividing the propertyValue of the home by 1,000, then multiplying the result by $3.50.
        annual = annual + (propElem.monthlyInsurance*12)
        total = annual + (monthly * 12)

        return total
    #
    def _monthlyMortgageFixedRate(self, propElem):
        monthsInTerm = propElem.mortgageYears*12
        c = propElem.mortgageInterest/12

        loanAmount = propElem.propertyValue * (1-propElem.downpayment)
        fixedMonthlyPayment = loanAmount * (c*(1+c)**monthsInTerm)
        fixedMonthlyPayment = fixedMonthlyPayment/((1+c)**monthsInTerm-1)
        return fixedMonthlyPayment
    #
    def _annualMarket(self, Capital, yearNumber, yearBearEnd = 5, bearMarketSimulation = True):
        
        if bearMarketSimulation and (yearNumber in range(0,yearBearEnd)):
            bear = Capital*(1-self.bearMarketDownturnPercentage)
            return bear
        else:
            noBear = (1 + self.marketPercentageReturn)*Capital
            return noBear
    #
    def _annualPropertyManagementFees(self, propElem):
        return (propElem.averageMonthlyRent*propElem.numberOfFamily*12)*propElem.managementFeePercentage

    ##### MUST find equation for paid off principal
    def _propertyOwnedPrincipalValue(self, propElem, year):

        propElem.remainingMortgagePrincible = propElem.propertyValue*(1-propElem.downpayment)
        propElem.propertyPrincipalPayedOff = propElem.propertyValue*propElem.downpayment
        
        for x in range(propElem.yearAdd,(propElem.yearAdd + year)*12):
            mortgageMonthlyPayments = self._monthlyMortgageFixedRate(propElem)
            interestMonthlyPayment = propElem.remainingMortgagePrincible*(propElem.mortgageInterest/12)
            monthPrincipalPaid = mortgageMonthlyPayments - interestMonthlyPayment
            propElem.propertyPrincipalPayedOff += monthPrincipalPaid
            propElem.remainingMortgagePrincible -= monthPrincipalPaid
            if propElem.remainingMortgagePrincible <= 0:
                propElem.remainingMortgagePrincible = 0
                propElem.propertyPrincipalPayedOff = propElem.propertyValue

        propElem.propertyOwnedValueWithAppr = propElem.propertyPrincipalPayedOff*((1+propElem.annualValueAppreciationPercentage)**(year-propElem.yearAdd))
        # print('owned value with appr ' + str(propElem.propertyOwnedValueWithAppr) + ', year ' + str(year))

class MarketReference:

    def __init__(self, startingCapital, inflation, years, avgMarketPercentageReturns, savedIncome):
       
        market = []
        market.append(startingCapital)
        self.bearMarketDownturnPercentage = 0.04
        self.marketPercentageReturn = avgMarketPercentageReturns

        for yr in range(0,years):

            if not yr == 0:
                newMarket = self._annualMarket(
                                                Capital=market[yr-1],
                                                yearNumber=yr,
                                                )
                newMarket += savedIncome
                # newMarket = market[yr-1]*(1+avgMarketPercentageReturns) + savedIncome
                newMarket = newMarket*(1-inflation)
                market.append(newMarket)

        gr = GraphSimple()    
        gr.plotList(
            list=market,
            listLabel='market reference returns',
            chartTitle=''
        ) 
    
    def _annualMarket(self, Capital, yearNumber, yearBearEnd = 5, bearMarketSimulation = True):
        
        if bearMarketSimulation and (yearNumber in range(0,yearBearEnd)):
            bear = Capital*(1-self.bearMarketDownturnPercentage)
            return bear
        else:
            noBear = (1 + self.marketPercentageReturn)*Capital
            return noBear


startingCapital = 25000
inflation = 0.0322
years = 30
avgMarketPercentageReturns = 0.075
savedIncome = 10000

MarketReference(
    startingCapital = startingCapital,
    inflation = inflation,
    years = years,
    avgMarketPercentageReturns = avgMarketPercentageReturns,
    savedIncome = savedIncome,
    )

prop = PropertyHandle(
        startingCapital = startingCapital,
        inflation = inflation,
        years = years,
        avgMarketPercentageReturns = avgMarketPercentageReturns,
        savedIncome = savedIncome,
        )


prop.addProperty(
    propertyValue                   = 200000, 
    yearAdd                         = 0, 
    downpayment                     = 0.2, 
    numberOfFamily                  = 2, 
    averageMonthlyRent              = 1120, 
    occupancyPercentage             = 0.95, 
    managed                         = True, 
    mortgageInterest                = 0.045, 
    mortgageYears                   = 30, 
    monthlyUtilities                = 300, 
    monthlyHOAFees                  = 300, 
    monthlyInsurance                = (350*4/12),
    percentageYearlyTaxes           = 0.009, 
    percentageAnnualMaintenance     = 0.02,  
    managementFeePercentage         = 0.1, 
    propValAppreciationPercentage   = 0.0322
    )

prop.simulate('test')