import gmplot
from propertyDatabase import *


# Store our latitude and longitude (San Antonio as example)
latitudes = []
longitudes = []

dbInter = dbInteractions()

# zipList = dbInter.dbGetZipList(78233)
zipList = dbInter.dbGetSA()
# print(zipList[1].gpsLat)

for x in range(0,len(zipList)):
    latitudes.append(zipList[x].gpsLat)    
    longitudes.append(zipList[x].gpsLon)

# latitudes.append(29.4241)
# longitudes.append(-98.4936)

# Creating the location we would like to initialize the focus on. 
# Parameters: Lattitude, Longitude, Zoom
gmap = gmplot.GoogleMapPlotter(29.4241, -98.4936, 10)

# Overlay our datapoints onto the map
gmap.heatmap(latitudes, longitudes)

# Generate the heatmap into an HTML file
gmap.draw("my_heatmap.html")
