from __future__ import print_function
import xlwings as xw  
from propertyDatabase import objProperty, dbInteractions, QueryType
import datetime

class excel(object):

    def __init__(self, excelPath, sheetName):
        self.path = excelPath
        self.wb = self._WorkbookConnect(excelPath)
        self.st = self.wb.sheets[sheetName]
    
    def _WorkbookConnect(self, path):
        wb = xw.Book(path)
        return wb

    def getCell(self, cell):
        return self.st.range(cell).value

    def setCell(self, cell, value):
        self.st.range(cell).value = value


class OpenAddress:

    def __init__(self, excel,
                lonCol, latCol, numberCol, streetCol, zipCol, cityCol):
        self.xlObj = excel
        self.lonCol = lonCol
        self.latCol = latCol
        self.numberCol = numberCol
        self.streetCol = streetCol
        self.zipCol = zipCol
        self.cityCol = cityCol

    def getLon(self, row):
        cell = self.lonCol + str(row)
        return self.xlObj.getCell(cell)
    
    def getLat(self, row):
        cell = self.latCol + str(row)
        return self.xlObj.getCell(cell)

    def getNumber(self, row):
        cell = self.numberCol + str(row)
        return self.xlObj.getCell(cell)

    def getStreet(self, row):
        cell = self.streetCol + str(row)
        return self.xlObj.getCell(cell)
    
    def getZip(self, row):
        cell = self.zipCol + str(row)
        return self.xlObj.getCell(cell)

    def getCity(self, row):
        cell = self.cityCol + str(row)
        return self.xlObj.getCell(cell)

    def setCell(self, cell, value):
        return self.xlObj.setCell(cell, value)


# Populate cities from openAccess (data preprocessing step)
xl = excel('C:\Users\Ethan\Desktop\OpenAddressProcessing\san_antonio.xlsx','san_antonio') # works


oa = OpenAddress(
        excel = xl,
        lonCol = 'A', 
        latCol = 'B', 
        numberCol = 'C', 
        streetCol = 'D', 
        zipCol = 'E', 
        cityCol = 'F'
        )


dbInteraction = dbInteractions()


def updateTableWithAnyThatDontExist():
    print('The following lines were inserted newly')
    for x in range(2,567152):
        prop = objProperty( 
                    streetAddress=oa.getNumber(x), 
                    streetName=oa.getStreet(x), 
                    stateCode='TX', 
                    zipCode=oa.getZip(x), 
                    city='San Antonio', 
                    gpsLat=oa.getLat(x), 
                    gpsLon=oa.getLon(x), 
                    sqft=None,
                    stories=None, 
                    rooms=None, 
                    bathrooms=None, 
                    buildyear=None, 
                    numberFamilies=None, 
                    numberBuildings=None,
                    HOADues=None, 
                    PropertyTaxes=None, 
                    Leins=None,
                    walkScore=None, 
                    crimeScore=None, 
                    schoolScore=None,
                    presentValue=None, 
                    presentRent=None
                    )
        
        query = dbInteraction.dbSingleInsertOrUpdate(prop)

        if query is QueryType.Insert:
            print(str(x) + ',', end='')


updateTableWithAnyThatDontExist()

def exampleInsertExcel():
    ranges = []
    cityData = []
    for x in range(0,120):
        ranges.append(5000*x + 2)
        ranges.append(5000*(x+1) + 1)

    for y in range(0,120):
        
        for x in range(ranges[2*y],ranges[2*y+1]):

            if x <= 567152:
                new = {'streetAddress' : oa.getNumber(x), 'streetName' : oa.getStreet(x),
                    'stateCode' : 'TX', 'zipCode' : oa.getZip(x), 'city' : 'San Antonio',
                    'gpsLat' : oa.getLat(x), 'gpsLon' : oa.getLon(x)}

                cityData.append(new)

        dbInteraction.dbMultipleBasicInsert(cityData)
        del cityData[:]





# # The nuclear, slow option (actually at least 20 times slower)
# for x in range(2,2000):#567152):

#     testProperty = objProperty( 
#         streetAddress = oa.getNumber(x),
#         streetName = oa.getStreet(x),
#         stateCode = 'TX',
#         zipCode = None,
#         city = 'San Antonio',
#         gpsLat = oa.getLat(x),
#         gpsLon = oa.getLon(x),
#         sqft = None,
#         stories = None,
#         rooms = None,
#         bathrooms = None, 
#         buildyear = None,
#         numberFamilies = None,
#         numberBuildings = None,
#         walkScore = None,
#         crimeScore = None,
#         schoolScore = None,
#         HOADues = None,
#         PropertyTaxes = None,
#         Leins = None,
#         presentValue = None,
#         presentRent = None
#         )

#     dbInteraction.dbSingleInsertOrUpdate(testProperty)























##############################
## Examples from previous use 
##############################


# The indented code below completed successfully in an hour!
    # xlZip = excel('C:\Users\Ethan\Desktop\OpenAddressProcessing\zip_code_san_antonio.xlsx','zip_code_san_antonio') # works

    # oaZip = OpenAddress(
    #         excel = xlZip,
    #         lonCol = 'G', 
    #         latCol = 'F', 
    #         numberCol = '', 
    #         streetCol = '', 
    #         zipCol = 'A', 
    #         cityCol = 'D'
    #         )


    # zipList = []

    # for y in range(2,89):
    #     zipCode = oaZip.getZip(y)
    #     latZip = oaZip.getLat(y)
    #     lonZip = oaZip.getLon(y)

    #     zipList.append((latZip, lonZip, zipCode))

    # # Based on some rough math this will take one hour to complete

    # for x in range(2,567152):
    #     lat = oa.getLat(x)
    #     lon = oa.getLon(x)

    #     smallest = (100, 0000)

    #     for z in range(0,len(zipList)):
    #         latAbs = abs(lat-zipList[z][0])
    #         lonAbs = abs(lon-zipList[z][1])
    #         new = ((latAbs+lonAbs),zipList[z][2])

    #         if new < smallest:
    #             smallest = new

    #     zipCell = 'E' + str(x)
    #     oa.setCell(zipCell,smallest[1])




